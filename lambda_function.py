import os
import json
import time
import pytz

import requests
import urllib3
import pprint
import psycopg2

from datetime import datetime, timedelta
from psycopg2.extras import execute_values

from msteams_webhooks import TeamsWebhook
from msteams_webhooks.actions import OpenURLAction
from msteams_webhooks.cards import AdaptiveCard
from msteams_webhooks.elements import TextBlock
from msteams_webhooks.containers import Container

# # Vô hiệu hóa cảnh báo chứng chỉ tự ký (self-signed certificate)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# ELASTICSEARCH_URL = "https://nprod-elasticsearch-master.mcredit.com.vn:443/*apm-*"
# ELASTICSEARCH_USERNAME = 'grafana_view'
# ELASTICSEARCH_PASSWORD = 'mT%7rF2#pRXK'


ELASTICSEARCH_URL = "https://10.23.140.77:9200/mc-waflogs-*"
ELASTICSEARCH_USERNAME = 'grafana_view'
ELASTICSEARCH_PASSWORD = 'g$xLM^H27p#x'
    

class DataRetriever:
    
    def __init__(self):
        # Elasticsearch settings
        self.url = ELASTICSEARCH_URL
        self.username = ELASTICSEARCH_USERNAME
        self.password = ELASTICSEARCH_PASSWORD
        
    def connect(self):
        try:
            conn = psycopg2.connect(
                database ="mclog-report", 
                user = "mclog_user",
                password = "3e#4r**vij9A", 
                host = "prod-share-service-rds-postgres-grafanadb.cdssahhwp4bu.ap-southeast-1.rds.amazonaws.com", 
                port = "5432"
            )
            
            # conn = psycopg2.connect(
            #     host="127.0.0.1",
            #     port="5432",
            #     database="postgres",
            #     user="postgres",
            #     password="1234"
            # )
            
            return conn
        except (Exception, psycopg2.DatabaseError) as error:
            print("Failed to connect to PostgreSQL", error)

    def create_database_and_table(self, table_name):
        connection = self.connect()
        cursor = connection.cursor()
        # Create a table with columns to match the keys in the dictionary
        cursor.execute(f'''
            CREATE TABLE IF NOT EXISTS public.{table_name} (
                timestamp TIMESTAMP,
                utc TIMESTAMP,
                "domain" TEXT,
                "req_payload" TEXT,
                "url" TEXT,
                "count" INTEGER,
                "time" TEXT,
                "type" TEXT,
                "location" TEXT
            )
        ''')

        connection.commit()
        connection.close()

    def fetch_data(self, query):
        connection = self.connect()
        cursor = connection.cursor()
        result = []

        try:
            cursor.execute(query)
            rows = cursor.fetchall()
            columns = [desc[0] for desc in cursor.description]

            for row in rows:
                row_dict = dict(zip(columns, row))
                result.append(row_dict)

        except (Exception, psycopg2.Error) as error:
            print("Error:", error)
        finally:
            if connection:
                cursor.close()
                connection.close()
        return result
    
    def get_timestamp_last(self):
        connection = self.connect()
        cursor = connection.cursor()
        
        # select last timestamp from table in database
        cursor.execute('''
            SELECT "timestamp"
            FROM public.historical_log_api_data
            ORDER BY "timestamp" DESC
            LIMIT 1;
        ''')

        # Lấy kết quả
        result = cursor.fetchone()
        # print("result", result)
        if result:
            latest_timestamp = result[0]
            # print("Latest timestamp:", latest_timestamp)
            return latest_timestamp
        else:
            print("No results found")

        # Đóng kết nối
        cursor.close()
        connection.close()

    def get_all_http_slow_1minutes_data(self, timestamp):
        query = f"""
            SELECT * FROM public.op_all_http_slow_1minutes_data
            WHERE type = 'http_slow_5_10s' or type = 'http_slow_10_20s' 
                or type = 'http_slow_20_30s' or type = 'http_slow_30s_least'
            AND "timestamp" > '{timestamp}'
            ORDER BY timestamp DESC
            LIMIT 9;
        """
        return self.fetch_data(query)

    def get_all_http_code_1minutes_data(self, timestamp):
        query = f"""
            SELECT * FROM public.op_all_http_code_1minutes_data
            WHERE type = 'http_500'
            AND "timestamp" > '{timestamp}'
            ORDER BY timestamp DESC
            LIMIT 9;
        """
        return self.fetch_data(query)

    def alter_table_add_uuid(self, table_name, column_name):
        connection = self.connect()
        cursor = connection.cursor()
        query = f"""
            CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";
            ALTER TABLE {table_name} ADD COLUMN {column_name} UUID;
            ALTER TABLE {table_name} 
            ALTER COLUMN {column_name} SET DEFAULT uuid_generate_v4();;
            
            UPDATE {table_name} 
            SET {column_name} = uuid_generate_v4() 
            WHERE {column_name} IS NULL;
            
            ALTER TABLE {table_name} 
            ADD PRIMARY KEY ({column_name});
        """
        cursor.execute(query)
        connection.commit()
        connection.close()

    def insert_log_api_data(self, result_list):
        print("Starting to insert log api data")
        connection = self.connect()
        cursor = connection.cursor()
        
        # self.alter_table_add_uuid('historical_log_api_data', 'id')
        # Prepare data for insertion
        data = []
        for result in result_list:
            row = (
                result["timestamp"],
                result["utc"],
                result["domain"],
                result["req_payload"],
                result["url"],
                result["count"],
                result["time"],
                result["type"],
                result["location"]
            )
            data.append(row)

        insert_query = '''
            INSERT INTO public.historical_log_api_data (
                timestamp, utc, domain, req_payload, url, count, time, type, location
            )
            VALUES %s
            ON CONFLICT DO NOTHING;'''

        execute_values(cursor, insert_query, data)

        print("Records inserted successfully log api !!!!")
        connection.commit()
        connection.close()
 



class MSTeamsWebhook:
    
    def __init__(self, url_webhook, time_now, time_utc):
        self.time_now = time_now
        self.time_utc = time_utc
        self.url_webhook = url_webhook
        self.channel = TeamsWebhook(self.url_webhook)
        
    def data_retriever(self):
        return DataRetriever()

    def templates_http_code(self, data_list):
        if not data_list:  # Kiểm tra xem danh sách dữ liệu có rỗng hay không
            print("No data to send")
            return
            
        card_containers = []
        msteams_list = []
        block_details = OpenURLAction(title=" Detail ", url="https://cloudmonitor.mcredit.com.vn/d/d75808ed-47da-4f29-8701-3de15960daff1/api-real-time-monitoring?orgId=1&refresh=30s&viewPanel=6")  # noqa: E501
        # print("Data list :", data_list)
        # print("Run")
        result_list = []
        for item in data_list:
            resp = item['resp']
            domain = item['domain']
            count = item['count']
            api = item['url.path']
            time = item['timestamp'].strftime("%Y-%m-%d %H:%M:%S")
            type = item['type']

            block_title     = TextBlock(text=f"**🔔 [Alerting] Http Code = 500**", wrap = True, weight="bolder", font_type = "extraLarge")  # noqa: F541, E501
            block_trace_id  = TextBlock(text=f"**Domain**: {domain}", wrap=True)
            block_message   = TextBlock(text=f"**Message**:  {resp}", wrap=True)
            block_api       = TextBlock(text=f"**API**:  {api}", wrap = True)
            block_service   = TextBlock(text=f"**Count**:  {count}", wrap = True)
            block_time      = TextBlock(text=f"**Time**:  {time}", wrap = True)
            
            container = Container(
                items=[block_title, block_trace_id, block_message, block_api, block_service, block_time],  # noqa: E501
                vertical_content_alignment="center",
                # style="attention",
                min_height="150px",
                # bleed=True,
                select_action = block_details,
                rtl=False,
            )
            card_containers.append(container)
            # Create the AdaptiveCard containing the list of containers
            
            result = {
                "timestamp" : self.time_now,
                "utc": self.time_utc,
                "domain": domain, 
                "req_payload": resp, 
                "url" : api, 
                "count": count, 
                "time": time, 
                "type": type, 
                "location": "on_prem"
            }
            result_list.append(result)
        
        msteams_list = {
            "width": "Full"
        }
        # print("Step")      
        adaptive_card = AdaptiveCard(body=card_containers, msteams = msteams_list, actions= [block_details])  # noqa: E501
        # Send the adaptive card
        # pprint.pprint(adaptive_card.serialize())
        self.channel.send_card(adaptive_card)
        print("Send success http code !!!")
        print("Result :", result_list)
        data_retriever = self.data_retriever()
        data_retriever.insert_log_api_data(result_list)

    def templates_http_slow(self, data_list):
        if not data_list:  # Kiểm tra xem danh sách dữ liệu có rỗng hay không
            ("No data to send")
            return
        # print("aaaaaaaaaaaaaaaa")
        card_containers = []
        msteams_list = []
        block_details = OpenURLAction(title=" Detail ", url="https://cloudmonitor.mcredit.com.vn/d/d75808ed-47da-4f29-8701-3de15960daff1/api-real-time-monitoring?orgId=1&refresh=30s&viewPanel=27")  # noqa: E501
        
        result_list= []
        for item in data_list:
            domain = item['domain']
            req_payload = item['req.payload']
            # author = item['author'].lower()
            api = item['url.path']
            count = item['count']
            time = item['timestamp'].strftime("%Y-%m-%d %H:%M:%S")
            type = item['type']
            
            block_title     = TextBlock(text=f"**🔔 [Alerting] Http Query Slow : 5 (s)**", wrap = True, weight="bolder", font_type = "extraLarge")  # noqa: F541, E501 # noqa: F541, E501
            block_trace_id  = TextBlock(text=f"**Domain** : {domain}", wrap = True)
            block_message   = TextBlock(text=f"**Payload** :  {req_payload}", wrap=True)
            block_api       = TextBlock(text=f"**API**:  {api}", wrap = True)
            block_service   = TextBlock(text=f"**Count**:  {count}", wrap = True)
            block_time      = TextBlock(text=f"**Time**:  {time}", wrap = True)
            # Kết hợp các TextBlock thành một biến message
            # message = f"{block_trace_id.text}\n{block_message.text}\n{block_api.text}\n{block_service.text}\n{block_time.text}"
            
            container = Container(
                items=[block_title, block_trace_id, block_message, block_api, block_service, block_time],  # noqa: E501
                vertical_content_alignment="center",
                # style="attention",
                min_height="150px",
                # bleed=True,
                select_action = block_details,
                rtl=False,
            )
            card_containers.append(container)
            # Create the AdaptiveCard containing the list of containers
             # Create a list of inserted data 
            result = {
                "timestamp" : self.time_now,
                "utc": self.time_utc,
                "domain": domain, 
                "req_payload": req_payload, 
                "url" : api, 
                "count": count, 
                "time": time, 
                "type": type, 
                "location": "on_prem"
            }
            result_list.append(result)
                    
        msteams_list = {
            # "entities": msteams_entity,
            "width": "Full"
        }
                    
        adaptive_card = AdaptiveCard(body=card_containers, msteams = msteams_list, actions= [block_details])  # noqa: E501
        # Send the adaptive card
        pprint.pprint(adaptive_card.serialize())
        self.channel.send_card(adaptive_card)
        print("Send success http slow !!!")
        print("Result :", result_list)
        data_retriever = self.data_retriever()
        data_retriever.insert_log_api_data(result_list)

  
def format_datetime_utc(dt):
    if not isinstance(dt, datetime):
        raise ValueError("Input must be a datetime.datetime object")

    # Convert datetime to ISO 8601 format
    local_timezone = pytz.timezone("Asia/Bangkok")  # Thay bằng múi giờ cục bộ của bạn
    # Convert the local datetime to UTC
    dt_localized = local_timezone.localize(dt)
    utc_datetime = dt_localized.astimezone(pytz.utc)
    utc_datetime = utc_datetime.replace(tzinfo=None)

    return utc_datetime

        
def lambda_handler(event, context):
    now = datetime.now(pytz.utc)
    #Define the utc+7
    utc_plus_7 = pytz.timezone('Asia/Bangkok')
    time_now = now.astimezone(utc_plus_7)
    time_now = time_now.replace(second=0).strftime('%Y-%m-%d %H:%M:%S') 
    
    # Get l timestamp in table 
    data_retriever = DataRetriever()
    data_retriever.create_database_and_table('historical_log_api_data')
    time_last = str(data_retriever.get_timestamp_last())

    time_lastest = (now.astimezone(utc_plus_7).replace(second=0) - timedelta(minutes=3)).strftime('%Y-%m-%d %H:%M:%S') if time_last =='None' else time_last  # noqa: E501
    print("Time now :", time_now)
    print("Time lastest :", time_lastest)
    # Chuyển đổi time_now thành đối tượng datetime
    time_now_datetime = datetime.strptime(time_now, '%Y-%m-%d %H:%M:%S')
    # Gọi hàm format_datetime_utc với đối tượng datetime
    time_utc = format_datetime_utc(time_now_datetime)

    if time_now > time_lastest:
        
        start_date = time_lastest
        end_date = time_now
        print("Time start : {} - {} ".format(start_date, end_date))

        # Validate start_date and end_date
        if start_date >= end_date:
            print("Error: Invalid date range. The start date must be earlier than the end date.")
            exit(1)
        start_time = time.time()  # Record the start time
        
        data_http_slow = data_retriever.get_all_http_slow_1minutes_data(time_lastest)
        data_http_code = data_retriever.get_all_http_code_1minutes_data(time_lastest)
       
        # Define a list of paths and services to check
        paths_to_check = [
            '/api/v1.0/mobile_4_sale_phase2/report/third-party',
            '/api/v1.0/mobile_4_sale_phase2/report-mobile-day',
            '/api/v1.0/mobile_4_sale_phase2/getLoanStatus',
            '/api/v1.0/mobile_4_sale_phase2/report-mobile-week',
            '/api/v1.0/mobile_4_sale_phase2/report-mobile-day-ver2',
            '/api/v1.0/mobile_4_sale_phase2/debt/detail',
            '/api/v1.0/mobile_4_sale_phase2/report-mobile-week-ver3',
            '/api/v1.0/customer/getDisbursementContractLimitList',
            '/api/v1.0/mobile_4_sale_phase2/debt',
            '/api/v1.0/mcportal/getTotalAMTLoanStatus',
            '/api/v1.0/mobile_4_sale_phase2/debt-v2',
            '/api/v1.0/disbursement/totalOSBalance',
            '/api/v1.0/contract_duplicate/checkFrameNo',
            '/api/v1.0/contract_limit/dueDate',
            '/api/v2.0/contract_limit/transfer',
            '/api/v1.0/contract_limit/transfer_v2',
            '/api/v1.0/token/refesh-token',
            '/api/v1.0/mcportal/refesh-token',
            '/api/v1.0/token/refesh-token',
            '/api/v1.0/mcportal/refesh-token',
            '/api/v1.0/bpm/checkCase',
            '/api/v1.0/customer/credit',
            '/api/v1.0/mobile_4_sale_phase2/getVBTDStatus',
            '/api/v1.0/bpm/contractDetail',
            '/api/v1.0/bpm/searchCase',
            '/api/v1.0/cic/duplicateDWH',
            '/api/v1.0/report/tableDocumentCreditStatus',
            '/api/v1.0/debt_collection/dueDateAndStatusPaid',
            '/api/v1.0/report/chartDocumentStatusByHour',
            '/api/v1.0/report/chartDocumentStatusByDay',
            '/api/v1.0/report/documentStatusMappingByChannel',
            '/api/v1.0/mcportal/duplicateContract',
            '/api/v1.0/report/customerType',
            '/api/v1.0/customer/eContract/credit',
            '/api/v1.0/report/eContractReport',
            '/api/v3.0/mobile_4_sales/mbal/getCustomerInMC',
            '/api/v1.0/report/searchContract',
            '/api/v1.0/report/checkDuplicateBPMNew',
            '/api/v1.0/disbursement/totalOSBalanceOther',
            '/api/v1.0/report/spcRevenueReport',
            '/api/v1.0/bpm/checkDuplicateTttd',
            '/api/v1.0/report/spcCustomerInfo',
            '/api/v1.0/bpm_new/searchCaseBPM',
            '/api/v1.0/bpm_new/getLoanProfileInfo',
            '/api/v1.0/report/getCustomerHaveLimitContract',
            '/api/v1.0/report/spcDisbursementReport',
            '/api/v1.0/medusa/contract/getDataEntry',
            '/api/v2.0/mcportal/loan/loanList',
            '/api/v1.0/afc/getCustomerInfoNew',
            '/api/v2.0/mcportal/getEMIforBPM',
            '/api/v1.0/bpm_new/getCustomTypeBPMNew',
            '/api/v1.0/contract_limit/getDPDNhomNo',
            '/api/v2.0/mcportal/loan/loanListv1',
            '/api/v2.0/mcportal/contact/customerList',
            '/api/v2.0/mcportal/duplicateContractForBPMNew',
            '/api/v2.0/t24/debt_collection',
            '/api/v1.0/bpm_new/getCustomerInfoDup',
            '/api/v2.0/mcportal/listDoc',
            '/api/v2.0/mcportal/checkStatus',
            '/api/v1.0/mcportal/gendoc/dwh',
            '/api/v1.0/afc/activeContract',
            '/api/v1.0/card/checkUserAFC',
            '/api/v2.0/mcportal/reportPCB',
            '/api/v2.0/mcportal/checkInfo',
            '/api/v2.0/mcportal/PCBdetail',
            '/api/v1.0/digital-lending-new/check_CCCD',
            '/api/v1.0/digital-lending-new/checkFaurd',
            '/api/v1.0/digital-lending-new/checkFaurdPhoneNo',
            '/api/v1.0/customer/getContractInfo',
            '/api/v1.0/bpm_new/getCustomerInfoDupPOI',
            '/api/v1.0/bpm_new/getTotalLoanLimit',
            '/api/v1.0/digital-lending-new/checkIdNumberFraud',
            '/api/v1.0/digital-lending-new/checkDeviceID',
            '/api/v1.0/digital-lending-new/faceSearchByID',
            '/api/v1.0/digital-lending-new/faceSearchByURLs',
            '/api/v1.0/digital-lending-new/checkFaurdRejectCode',
            '/api/v1.0/bpm_new/checkFraudForBPMNew',
            '/api/v1.0/customer/getContractInfoV2',
            '/api/v2.0/mcportal/getPoiBPMNew',
        ]
        
        data_slow_api = []
        data_code_api = []
        print("Data http slow : ", data_http_slow) 
        print("Data http code : ", data_http_code) 
        
        for item in data_http_slow + data_http_code:
            if any(path in item['url.path'] for path in paths_to_check):
                if item in data_http_slow:
                    data_slow_api.append(item)
                else:
                    data_code_api.append(item)

        print("Data slow api: ", data_slow_api) 
        print("Data code api: ", data_code_api) 
        channel_url = "https://bghieumbbankcom.webhook.office.com/webhookb2/877d57a1-41b6-4ec1-8ba5-852bb6b508eb@2b931bbb-a7e2-453c-98f9-554f1ac0414d/IncomingWebhook/bfb817cee2324832bd85d4205b52544b/acff893f-3a62-40ec-b933-cf16251a69bf"
        # channel_url = "https://bghieumbbankcom.webhook.office.com/webhookb2/9c6c4064-21a2-472b-a318-094d8aae5006@2b931bbb-a7e2-453c-98f9-554f1ac0414d/IncomingWebhook/7e5c686ea9004eadbd54ce1aabdc23fd/b91daf27-0872-4acb-8827-be9ea3888768"
        teams_webhook = MSTeamsWebhook(channel_url, time_now, time_utc)

        teams_webhook.templates_http_slow(data_slow_api)
        teams_webhook.templates_http_code(data_code_api)
        
        end_time = time.time()  # Ghi lại thời gian kết thúc
        execution_time = end_time - start_time
        print(f"Execution time: {execution_time:.2f} seconds")
